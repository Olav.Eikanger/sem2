# Mitt program
Velkommen til Jump King!

For å styre karakteren, bruk høyre og venstre pil-tast for å bevege deg til høyre og venstre. For å hoppe, hold inne mellomrom. Dette lader opp hoppet. Når du føler for å hoppe, slipp mellomrom. Du kan også hoppe en spesifikk retning ved å holde inne piltastene mens du slipper mellomrom.

Kollisjonen i spillet virker ved å se på hvor stor fart du har og i hvilken retning. Den beregner da om du kommer til å kollidere med noen av linjene på skjermen. Hvis du kolliderer med noe som ikke er flatt, vil du sprette av. Det skal nevnes at spillet har to forskjellige metoder å oppdage kollisjoner på, men denne er standard. Hvis du ønsker å bytte over til den andre, kan du bytte ut collisionDetectionMethod i [main](src/main/java/no/uib/inf101/sem2/Main.java) til DISCRETE.

Kildene jeg brukte for dette prosjektet var:
[link](https://github.com/Code-Bullet/Jump-King/tree/321506e725ef448654936837672d9fe8fba123bb) Inspirasjon og grafikk assets
[link](https://en.wikipedia.org/wiki/Collision_detection#A_posteriori_(discrete)_versus_a_priori_(continuous)) Forskjellige måter å håndtere kollisjoner
[link](https://gamedevelopment.tutsplus.com/tutorials/collision-detection-using-the-separating-axis-theorem--gamedev-169) Hvordan SAT fungerer (Endte opp med å ikke være helt ideelt, så gikk over til min egen metode)

[Her er en kort videosnutt som forklarer spillet](https://drive.google.com/drive/folders/1euPT-o58O4tO3nvAn4w26nXZ-wjpdKzG?usp=share_link)

Se [oppgaveteksten](./OPPGAVETEKST.md) til semesteroppgave 2. Denne README -filen kan du endre som en del av dokumentasjonen til programmet, hvor du beskriver for en bruker hvordan programmet skal benyttes.