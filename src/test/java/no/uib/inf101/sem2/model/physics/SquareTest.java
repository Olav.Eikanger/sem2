package no.uib.inf101.sem2.model.physics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.physics.Square.Side;

public class SquareTest {
    @Test
    public void testSideLines() {
        Square square = Square.createRectangle(0, 0, 10, 10);
        Line topLine = square.getSide(Side.TOP);
        Line bottomLine = square.getSide(Side.BOTTOM);
        Line leftLine = square.getSide(Side.LEFT);
        Line rightLine = square.getSide(Side.RIGHT);
        assertEquals(new Line(new Point(0, 0), new Point(10, 0)), topLine);
        assertEquals(new Line(new Point(0, 10), new Point(10, 10)), bottomLine);
        assertEquals(new Line(new Point(0, 0), new Point(0, 10)), leftLine);
        assertEquals(new Line(new Point(10, 0), new Point(10, 10)), rightLine);
    }

    @Test
    public void testSquare() {
        Square square = Square.createRectangle(0, 5, 10, 20);
        assertEquals(0, square.getX());
        assertEquals(5, square.getY());
        assertEquals(10, square.getWidth());
        assertEquals(20, square.getHeight());
    }
}
