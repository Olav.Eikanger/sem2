package no.uib.inf101.sem2.model.physics;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.PlayerModel;
import no.uib.inf101.sem2.model.PlayerState;

public class CollidableTest {
  private Vector simulateFutureCollision(Vector velocity, Square rectangle, List<Line> lines) {
    int MAX_ITERATIONS = 100;
    Vector[] vectors;
    Vector finalPosition = Vector.ZERO;
    do {
        vectors = CollisionDetection.checkFutureCollisions(rectangle, velocity, lines);
        finalPosition = finalPosition.add(vectors[0]);
        rectangle = Square.createRectangle(
          rectangle.getX() + vectors[0].x(),
          rectangle.getY() + vectors[0].y(),
          rectangle.getWidth(),
          rectangle.getHeight()
        );
        velocity = vectors[1];
        MAX_ITERATIONS--;
    } while (!velocity.equals(Vector.ZERO) && MAX_ITERATIONS > 0);

    return finalPosition;
  }

  @Test
  public void testCollisionResolution1() {
    Square rectangle = Square.createRectangle(407.5, 230, 32.5, 35);
    Vector velocity = new Vector(-10, -10);
    List<Line> lines = List.of(
      new Line(new Point(440, 410), new Point(440, 230)),
      new Line(new Point(440, 230), new Point(590, 230))
    );

    Vector travel = simulateFutureCollision(velocity, rectangle, lines);
    assertEquals(new Vector(-10, -10), travel);
  }

  @Test
  public void testCollisionResolution2() {
    Square rectangle = Square.createRectangle(0, 0, 10, 10);
    Vector remainingVelocity = new Vector(10, -11);
    List<Line> lines = List.of(
      new Line(new Point(20, 0), new Point(10, 0)),
      new Line(new Point(10, 0), new Point(10, 10))
    );

    Vector travel = simulateFutureCollision(remainingVelocity, rectangle, lines);
    assertEquals(new Vector(-5, -11), travel);
  }

  @Test
  public void testCollisionResolution3() {
    Square rectangle = Square.createRectangle(0, 0, 10, 10);
    Vector remainingVelocity = new Vector(2, -4);
    List<Line> lines = List.of(
      new Line(new Point(11, -2), new Point(11, 8)),
      new Line(new Point(11, -2), new Point(21, -2))
    );

    Vector travel = simulateFutureCollision(remainingVelocity, rectangle, lines);
    assertEquals(new Vector(.5, -4), travel);
  }

  @Test
  public void testCollisionResolution4() {
    Square rectangle = Square.createRectangle(0, 0, 10, 10);
    Vector remainingVelocity = new Vector(2, -4);
    List<Line> lines = List.of(
      new Line(new Point(10, 6), new Point(10, 15))
    );

    Vector travel = simulateFutureCollision(remainingVelocity, rectangle, lines);
    assertEquals(new Vector(-1, -4), travel);
  }

  @Test
  public void testCollisionResolution5() {
    Square rectangle = Square.createRectangle(0, 0, 10, 10);
    Vector remainingVelocity = new Vector(3, 0);
    List<Line> lines = List.of(
      new Line(new Point(6, 10), new Point(6, 20)),
      new Line(new Point(6, 10), new Point(16, 10))
    );

    Vector travel = simulateFutureCollision(remainingVelocity, rectangle, lines);
    assertEquals(new Vector(3, 0), travel);
  }

  @Test
  public void testCollisionResolution6() {
    Square rectangle = Square.createRectangle(0, 0, 10, 10);
    Vector remainingVelocity = new Vector(5,5);
    List<Line> lines = List.of(
      new Line(new Point(10, 10), new Point(10, 30)),
      new Line(new Point(10, 10), new Point(30, 10))
    );

    Vector travel = simulateFutureCollision(remainingVelocity, rectangle, lines);
    assertEquals(new Vector(-2.5, 5), travel);
  }

  @Test
  public void testDiagonalCollition() {
    Square rectangle = Square.createRectangle(0, 0, 10, 10);
    Vector remainingVelocity = new Vector(5,5);
    List<Line> lines = List.of(
      new Line(new Point(0, 20), new Point(20, 0))
    );

    Vector travel = simulateFutureCollision(remainingVelocity, rectangle, lines);
    assertEquals(-2, Math.round(travel.x()));
    assertEquals(-2, Math.round(travel.y()));
  }

  @Test
  public void nonCollisionTest() {
    PlayerModel player = new PlayerModel(new Vector(-5, -5), new Vector(0, 0), new Vector(10, 10), new PlayerState());
    Line line1 = new Line(new Point(-10, 0), new Point(0, 10));
    Line line2 = new Line(new Point(0, 10), new Point(10, 0));
    Line line3 = new Line(new Point(10, 0), new Point(0, -10));
    Line line4 = new Line(new Point(0, -10), new Point(-10, 0));

    assertFalse(CollisionDetection.isCollidingWith(player.getShape(), line1));
    assertFalse(CollisionDetection.isCollidingWith(player.getShape(), line2));
    assertFalse(CollisionDetection.isCollidingWith(player.getShape(), line3));
    assertFalse(CollisionDetection.isCollidingWith(player.getShape(), line4));
  }

  @Test
  public void collisionPlatformTest() {
    Line line = new Line(new Point(0, 10), new Point(10, 10));
    PlayerModel player = new PlayerModel(new Vector(0, 4), new Vector(0, 0), new Vector(10, 10.5), null);
    assertTrue(CollisionDetection.isCollidingWith(player.getShape(), line));
    Optional<Vector> clipOffset = CollisionDetection.getMinimumClipOffset(player.getShape(), line);
    assertEquals(new Vector(-0d, -4.5), clipOffset.get());
    player.setPosition(player.getPosition().add(clipOffset.get()));
    assertFalse(CollisionDetection.isCollidingWith(player.getShape(), line));
  }

  @Test
  public void collisionWallTest() {
    Line line = new Line(new Point(10, 0), new Point(10, 10));
    PlayerModel player = new PlayerModel(new Vector(4, 0), new Vector(0, 0), new Vector(10, 10), new PlayerState());
    assertTrue(CollisionDetection.isCollidingWith(player.getShape(), line));
    Optional<Vector> clipOffset = CollisionDetection.getMinimumClipOffset(player.getShape(), line);
    assertEquals(new Vector(-4, 0), clipOffset.get());
    player.setPosition(player.getPosition().add(clipOffset.get()));
    assertFalse(CollisionDetection.isCollidingWith(player.getShape(), line));
  }

  @Test
  public void collisionDiagonalTest() {
    Line line = new Line(new Point(0, 0), new Point(10, 10));
    PlayerModel player = new PlayerModel(new Vector(1, -1), new Vector(0, 0), new Vector(10, 10), new PlayerState());
    assertTrue(CollisionDetection.isCollidingWith(player.getShape(), line));
    Optional<Vector> clipOffset = CollisionDetection.getMinimumClipOffset(player.getShape(), line);
    assertEquals(4, Math.round(clipOffset.get().x()));
    assertEquals(-4, Math.round(clipOffset.get().y()));
  }

  @Test
  public void standingOnGroundTest() {
    Line line = new Line(new Point(0, 10), new Point(10, 10));
    List<Line> lines = Arrays.asList(line);
    PlayerModel player = new PlayerModel(new Vector(0, 4), null, new Vector(10, 10), null);
    player.setPosition(
      player.getPosition().add(
        CollisionDetection.getMinimumClipOffset(player.getShape(), lines.get(0)).get()
      )
    );
    assertTrue(CollisionDetection.isStandingOnGround(player.getShape(), lines));
  }
}
