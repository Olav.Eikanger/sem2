package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.physics.Line;
import no.uib.inf101.sem2.model.physics.Point;

public class LevelModelTest {
    @Test
    public void testLevelCollidables() {
        Map<String, List<Line>> collidables = Map.of(
            "level0", List.of(
                new Line(new Point(0, 0), new Point(5, 0)),
                new Line(new Point(0, 0), new Point(0, 5))
            ),
            "level1", List.of(
                new Line(new Point(10, 10), new Point(15, 0)),
                new Line(new Point(10, 10), new Point(0, 15))
            )
        );
        LevelModel level = new LevelModel(0, collidables, 0, 0);
        assertEquals(level.getLevelCollidables(), collidables.get("level0"));
        level.nextLevel();
        assertEquals(level.getLevelCollidables(), collidables.get("level1"));
    }

    @Test
    public void sanityTest() {
        LevelModel level = new LevelModel(0, null, 0, 0);
        for (int i = 0; i < 10; i++) {
            assertEquals(level.getCurrentLevel(), i);
            level.nextLevel();
        }

        for (int i = 10; i >= 0; i--) {
            assertEquals(level.getCurrentLevel(), i);
            level.previousLevel();
        }
    }
}
