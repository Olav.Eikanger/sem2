package no.uib.inf101.sem2.model.physics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class LineTest {
    @Test
    public void sanityTest() {
        Line line1 = new Line(new Point(0, 10), new Point(20, 30));
        assertEquals(0, line1.getX());
        assertEquals(10, line1.getY());
        assertEquals(20, line1.getMaxX());
        assertEquals(30, line1.getMaxY());

        Line line2 = new Line(new Point(0, 10), new Point(-20, -30));
        assertEquals(-20, line2.getX());
        assertEquals(-30, line2.getY());
        assertEquals(0, line2.getMaxX());
        assertEquals(10, line2.getMaxY());
    }
}
