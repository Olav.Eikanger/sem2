package no.uib.inf101.sem2.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.awt.geom.Rectangle2D;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.physics.Vector;

public class ModelTest {
    @Test
    public void testGetPlayerBounds() {
        PlayerModel player = new PlayerModel(Vector.ZERO, null, new Vector(10, 10), null);
        Model model = new Model(player, null, null, null);
        Rectangle2D expectedBounds = new Rectangle2D.Double(0, 0, 10, 10);
        assertEquals(expectedBounds, model.getPlayerBounds());
    }

    @Test
    public void testGetLevelNumber() {
        for (int i = 0; i < 10; i++) {
            LevelModel level = new LevelModel(i, null, 0, 0);
            Model model = new Model(null, level, null, null);
            assertEquals(i, model.getLevelNumber());
        }
    }

    @Test
    public void testGetViewDirection() {
        ControllerInputs inputs = new ControllerInputs();
        Model model = new Model(null, null, inputs, null);
        assertEquals(Direction.NONE.coefficent, model.getViewDirection());
        model.setMoveLeft(true);
        assertEquals(Direction.LEFT.coefficent, model.getViewDirection());
        model.setMoveRight(true);
        assertEquals(Direction.NONE.coefficent, model.getViewDirection());
        model.setMoveLeft(false);
        assertEquals(Direction.RIGHT.coefficent, model.getViewDirection());
    }
}
