package no.uib.inf101.sem2.model.physics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class PointTest {
    @Test
    public void sanityTest() {
        Point point = new Point(0, 10);
        assertEquals(0, point.getX());
        assertEquals(10, point.getY());
        assertEquals(0, point.getMaxX());
        assertEquals(10, point.getMaxY());
    }
}
