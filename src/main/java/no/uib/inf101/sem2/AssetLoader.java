package no.uib.inf101.sem2;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

public class AssetLoader {
    protected static Map<String, Image> fetchImages(String imageDirectoryPath) throws IOException {
        String currentWorkingDirectory =
        AssetLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace("%20", " ");
        Map<String, Image> images = new HashMap<>();
        File imageDirectory = new File(currentWorkingDirectory + "/" + imageDirectoryPath);
        if (!imageDirectory.isDirectory()) {
            throw new IllegalArgumentException(imageDirectoryPath + " is not a directory");
        }

        for (File file : imageDirectory.listFiles()) {
          images.put("level" + (Integer.parseInt(file.getName().split("\\.")[0]) - 1), ImageIO.read(file));
        }
    
        return images;
    }

    protected static Image fetchImage(String path) throws IOException {
        String currentWorkingDirectory =
        AssetLoader.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace("%20", " ");
        return ImageIO.read(new File(currentWorkingDirectory + "/" + path));
    }
}
