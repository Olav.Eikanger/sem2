package no.uib.inf101.sem2.view;

import java.util.List;

import no.uib.inf101.sem2.model.physics.Line;

public interface ViewableLevel {
    /**
     * Gets the level number
     * @return An integer representing the current level
     */
    public int getCurrentLevel();

    /**
     * Gets all the collision lines in the current level
     * @return A list of collision lines
     */
    public List<Line> getLevelCollidables();
}
