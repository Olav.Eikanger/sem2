package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Map;

import no.uib.inf101.sem2.model.physics.Line;

public class LevelView {
    private ViewableLevel level;
    private Map<String, Image> levelImages;

    public LevelView(ViewableLevel level, Map<String, Image> levelImages) {
        this.level = level;
        this.levelImages = levelImages;
    }
    
    protected void drawLevel(Graphics2D g2, Dimension dimension) {
        g2.drawImage(
            levelImages.get("level" + level.getCurrentLevel()),
            0,
            0,
            (int) dimension.getWidth(),
            (int) dimension.getHeight(),
            null
        );
    }

    protected void drawLines(Graphics2D g2) {
        g2.setColor(Color.RED);
        for (Line line : level.getLevelCollidables()) {
            g2.drawLine(
                (int) line.x1(),
                (int) line.y1(),
                (int) line.x2(),
                (int) line.y2()
            );
        }
    }
}
