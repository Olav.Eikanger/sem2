package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Rectangle2D;

public class PlayerView {
    private ViewablePlayer player;
    private SpriteSheet spriteSheet;

    public PlayerView(ViewablePlayer player, SpriteSheet sprite) {
        this.player = player;
        this.spriteSheet = sprite;
    }

    protected void drawPlayer(Graphics2D g2) {
        Rectangle2D playerBounds = player.getBounds();
        Image sprite = spriteSheet.get(0, 0);
        double aspectRatio = (double) sprite.getWidth(null) / (double) sprite.getHeight(null);
        double spriteWidth = playerBounds.getHeight() * aspectRatio;
        double spriteStartX = playerBounds.getX() + (playerBounds.getWidth() - spriteWidth) / 2;
        g2.drawImage(
            sprite,
            (int) spriteStartX,
            (int) playerBounds.getY(),
            (int) (playerBounds.getHeight() * aspectRatio),
            (int) playerBounds.getHeight(),
            null
        );
    }

    protected void drawBounds(Graphics2D g2) {
        g2.setColor(Color.RED);
        g2.draw(player.getBounds());
    }
}
