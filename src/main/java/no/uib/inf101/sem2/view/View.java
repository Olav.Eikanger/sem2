package no.uib.inf101.sem2.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class View extends JPanel {
    private boolean testing = false;
    private ViewableModel model;
    private PlayerView player;
    private LevelView level;

    public View(ViewableModel model, PlayerView player, LevelView level, Dimension dimension) {
        this.model = model;
        this.player = player;
        this.level = level;
        this.setFocusable(true);
        this.setPreferredSize(dimension);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        level.drawLevel(g2, getSize());
        player.drawPlayer(g2);

        if (testing) {
            level.drawLines(g2);
            player.drawBounds(g2);
            displayHiddenInformation(g2);
        }
    }
    
    private void displayHiddenInformation(Graphics2D g2) {
        g2.setColor(Color.WHITE);
        g2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        int lineHeight = g2.getFontMetrics().getHeight();
        int currentBaseline = lineHeight;
        g2.drawString("isOnGround: " + String.valueOf(model.playerIsOnGround()), 10, currentBaseline);
        currentBaseline += lineHeight;
        g2.drawString("x: " + model.getPlayerBounds().getX(), 10, currentBaseline);
        currentBaseline += lineHeight;
        g2.drawString("y: " + model.getPlayerBounds().getY(), 10, currentBaseline); 
    }
}
