package no.uib.inf101.sem2.view;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class SpriteSheet {
    private Image sheet;
    private int tileWidth;
    private int tileHeight;

    public SpriteSheet(Image sheet, int tileWidth, int tileHeight) {
        this.sheet = sheet;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
    }

    protected Image get(int x, int y) {
        BufferedImage tile = new BufferedImage(tileWidth, tileHeight, BufferedImage.TYPE_4BYTE_ABGR);
        Graphics g = tile.getGraphics();
        g.drawImage(
            sheet,
            0,
            0,
            tileWidth,
            tileHeight,
            tileWidth * x,
            tileHeight * y,
            tileWidth * (x + 1),
            tileHeight * (y + 1),
            null
        );

        return tile;
    }
}
