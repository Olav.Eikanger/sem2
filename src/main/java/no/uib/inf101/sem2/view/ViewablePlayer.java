package no.uib.inf101.sem2.view;

import java.awt.geom.Rectangle2D;

public interface ViewablePlayer {
    public Rectangle2D getBounds();
}
