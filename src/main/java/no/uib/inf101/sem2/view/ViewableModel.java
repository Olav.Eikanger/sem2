package no.uib.inf101.sem2.view;

import java.awt.geom.Rectangle2D;

public interface ViewableModel {
    /**
     * Gets the player bounds
     */
    public Rectangle2D getPlayerBounds();

    /**
     * Gets the level number
     */
    public int getLevelNumber();

    /**
     * Gets the direction the player is facing
     * @return 1 for right, -1 for left
     */
    public int getViewDirection();

    /**
     * Checks if the player is standing on ground
     * @return True if it is, false otherwise
     */
    public boolean playerIsOnGround();
}
