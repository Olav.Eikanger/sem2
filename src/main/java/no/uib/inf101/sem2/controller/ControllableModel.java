package no.uib.inf101.sem2.controller;

public interface ControllableModel {
    /**
     * Updates the state of the model by one tick
     */
    public void doGameTick();

    /**
    * Indicates if the player should move to the left or not
    * The player can't move whilst not on the ground or charging a jump
    * @param value If the player should move left or not
    */
    public void setMoveLeft(boolean value);

    /**
    * Indicates if the player should move to the right or not
    * The player can't move whilst not on the ground or charging a jump
    * @param value If the player should move right or not
    */
    public void setMoveRight(boolean value);

    /**
     * Indicates that the player should start charging it's jump.
     * The method should be called once, followed by {@link #releaseJump() releaseJump}
     * to perform the charged jump
     */
    public void chargeJump();

    /**
     * Indicates that the player should perform it's charged jump.
     * This resets the jump strength of the player.
     * The moethod won't do anything if {@link #chargeJump() chargeJump} wasn't called first
     */
    public void releaseJump();
}
