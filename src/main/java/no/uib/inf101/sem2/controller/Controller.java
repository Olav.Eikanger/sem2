package no.uib.inf101.sem2.controller;

import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.Timer;
import no.uib.inf101.sem2.view.View;

public class Controller extends KeyAdapter {
    private static int TICKS_PER_SECOND = 50;

    private ControllableModel model;
    private View view;
    private Timer gameLoop;

    public Controller(ControllableModel model, View view) {
        this.model = model;
        this.view = view;
        view.addKeyListener(this);
        this.gameLoop = new Timer(1000 / TICKS_PER_SECOND, this::gameTick);
        gameLoop.start();
    }

    private void gameTick(ActionEvent event) {
        model.doGameTick();
        view.repaint();
    }

    @Override
    public void keyPressed(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.VK_LEFT -> model.setMoveLeft(true);
            case KeyEvent.VK_RIGHT -> model.setMoveRight(true);
            case KeyEvent.VK_SPACE -> model.chargeJump();
            case KeyEvent.VK_T -> {
                if (gameLoop.isRunning()) {
                    gameLoop.stop();
                } else {
                    gameLoop.start();
                }
            }
            case KeyEvent.VK_B -> gameTick(null);
        }
    }

    @Override
    public void keyReleased(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.VK_LEFT -> model.setMoveLeft(false);
            case KeyEvent.VK_RIGHT -> model.setMoveRight(false);
            case KeyEvent.VK_SPACE -> model.releaseJump();
        }
    }
}