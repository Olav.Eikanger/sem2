package no.uib.inf101.sem2;

import java.awt.Dimension;
import java.awt.Image;
import java.util.List;
import java.util.Map;
import java.io.IOException;

import javax.swing.JFrame;

import no.uib.inf101.sem2.controller.Controller;
import no.uib.inf101.sem2.model.ControllerInputs;
import no.uib.inf101.sem2.model.LevelModel;
import no.uib.inf101.sem2.model.Model;
import no.uib.inf101.sem2.model.PlayerModel;
import no.uib.inf101.sem2.model.PlayerState;
import no.uib.inf101.sem2.model.physics.Line;
import no.uib.inf101.sem2.model.physics.Vector;
import no.uib.inf101.sem2.view.LevelView;
import no.uib.inf101.sem2.view.PlayerView;
import no.uib.inf101.sem2.view.SpriteSheet;
import no.uib.inf101.sem2.view.View;

public class Main {
  private static int WIDTH = 600;
  private static int HEIGHT = 450;
  private static double PLAYER_WIDTH = 32.5;
  private static double PLAYER_HEIGHT = 35;
  private static String BACKGROUND_IMAGE_DIRECTORY = "level_images";
  private static String LEVEL_DIRECTORY = "levels";
  private static String PLAYER_PATH = "/player_assets/base.png";
  public static void main(String[] args) throws IOException {
    Map<String, List<Line>> levels = LevelLoader.fetchLevels(LEVEL_DIRECTORY);
    LevelModel levelModel = new LevelModel(0, levels, WIDTH, HEIGHT);
    PlayerModel playerModel =
    new PlayerModel(new Vector(400, 90), Vector.ZERO, new Vector(PLAYER_WIDTH, PLAYER_HEIGHT), new PlayerState());
    Model model = new Model(
      playerModel,
      levelModel,
      new ControllerInputs(),
      Model.CollisionDetectionMethod.CONTINOUS
    );

    Map<String, Image> levelImages = AssetLoader.fetchImages(BACKGROUND_IMAGE_DIRECTORY);
    Image spriteSheetImage = AssetLoader.fetchImage(PLAYER_PATH);
    SpriteSheet spriteSheet = new SpriteSheet(spriteSheetImage, 31, 32);
    PlayerView playerView = new PlayerView(playerModel, spriteSheet);
    LevelView levelView = new LevelView(levelModel, levelImages);
    View view = new View(model, playerView, levelView, new Dimension(WIDTH, HEIGHT));
    Controller controller = new Controller(model, view);
    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Jump King");
    frame.setContentPane(view);
    frame.setResizable(false);
    frame.pack();
    frame.setVisible(true);
  }
}
