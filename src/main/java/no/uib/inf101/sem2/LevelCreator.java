package no.uib.inf101.sem2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

import no.uib.inf101.sem2.model.physics.Line;
import no.uib.inf101.sem2.model.physics.Point;

public class LevelCreator {
    private static int WIDTH = 600;
    private static int HEIGHT = 450;
    private static String IMAGE_DIRECTORY = "level_images";
    private static String LEVEL_DIRECTORY = "levels";
    private static String TEMP_LEVEL_DIRECTORY = "levels";
    private static int currentLevel = 0;
    public static void main(String[] args) throws IOException {
        Map<String, Image> images = AssetLoader.fetchImages(IMAGE_DIRECTORY);
        Map<String, List<Line>> levels = LevelLoader.fetchLevels(LEVEL_DIRECTORY);

        JPanel view = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g;
                g2.drawImage(
                    images.get("level" + currentLevel),
                    0,
                    0,
                    LevelCreator.WIDTH,
                    LevelCreator.HEIGHT,
                    null
                );

                if (!levels.containsKey("level" + currentLevel)) {
                    return;
                }
                
                g2.setColor(Color.RED);
                for (Line line : levels.get("level" + currentLevel)) {
                    g2.drawLine(
                        (int) line.x1(),
                        (int) line.y1(),
                        (int) line.x2(),
                        (int) line.y2()
                    );
                }
            }
        };

        MouseAdapter clickListener = new MouseAdapter() {
            Point previousClickedPosition = new Point(0, 0);
            boolean isDrawingLine = false;

            @Override
            public void mouseClicked(MouseEvent event) {
                if (!levels.containsKey("level" + currentLevel)) {
                    levels.put("level" + currentLevel, new ArrayList<>());
                }

                System.out.println("x: " + event.getX());
                System.out.println("y: " + event.getY());
                System.out.println("rx: " + round(event.getX()));
                System.out.println("ry: " + round(event.getY()));
                if (isDrawingLine) {
                    levels.get("level" + currentLevel).add(new Line(
                        previousClickedPosition,
                        new Point(
                            round(event.getX()),
                            round(event.getY())
                        )
                    ));

                    view.repaint();
                } else {
                    previousClickedPosition = new Point(round(event.getX()), round(event.getY()));
                }

                isDrawingLine = !isDrawingLine;
            }

            // Rounds to closest 10
            private int round(int num) {
                return num + (5 - (num + 5) % 10) % 10;
            }
        };

        KeyAdapter keyListener = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent event) {
                switch (event.getKeyCode()) {
                    case KeyEvent.VK_UP -> currentLevel++;
                    case KeyEvent.VK_DOWN -> currentLevel--;
                    case KeyEvent.VK_S -> {
                        if (event.isControlDown()) {
                            try {
                                LevelLoader.saveLevels(levels, TEMP_LEVEL_DIRECTORY);
                            } catch (IOException e) {
                                System.err.println("Failed to save levels");
                                e.printStackTrace();
                            }
                        }
                    }
                    case KeyEvent.VK_Z -> {
                        if (event.isControlDown()) {
                            List<Line> list = levels.get("level" + currentLevel); 
                            list.remove(list.size() - 1);
                        }
                    }
                }
                
                view.repaint();
            }
        };

        view.addMouseListener(clickListener);
        view.addKeyListener(keyListener);
        view.setPreferredSize(new Dimension(600, 450));
        view.setFocusable(true);
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Jump King");
        frame.setContentPane(view);
        frame.pack();
        frame.setVisible(true);
    }
}
