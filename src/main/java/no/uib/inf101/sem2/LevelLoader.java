package no.uib.inf101.sem2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import no.uib.inf101.sem2.model.physics.Line;
import no.uib.inf101.sem2.model.physics.Point;

public class LevelLoader {
    protected static Map<String, List<Line>> fetchLevels(String path) throws NumberFormatException, IOException {
        Map<String, List<Line>> levels = new HashMap<>();
        String filePath = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace("%20", " ");
        File directory = new File(filePath + "/" + path);
        for (File file : directory.listFiles()) {
            BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                    new FileInputStream(file)
                )
            );
            
            List<Line> lines = new ArrayList<>();
            for (String line = reader.readLine(); line != null; line = reader.readLine()) {
                double[] linePoints = new double[4];
                String[] entries = line.split(",");
                for (int i = 0; i < entries.length; i++) {
                    linePoints[i] = Integer.parseInt(entries[i]);
                }
                
                Line collisionLine =
                new Line(new Point(linePoints[0], linePoints[1]), new Point(linePoints[2], linePoints[3]));
                lines.add(collisionLine);
            }

            levels.put(file.getName().split("\\.")[0], lines);
            reader.close();
        }

        return levels;
    }

    protected static void saveLevels(Map<String, List<Line>> levels, String path) throws IOException {
        String filePath = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath().replace("%20", " ");;
        File directory = new File(filePath + "/" + path);
        for (String levelName : levels.keySet()) {
            BufferedWriter writer = new BufferedWriter(
                new OutputStreamWriter(
                    new FileOutputStream(directory + "\\" + levelName + ".csv")
                )
            );

            List<Line> lines = levels.get(levelName);
            for (Line line : lines) {
                String writeString = 
                (int) line.x1() + "," + 
                (int) line.y1() + "," + 
                (int) line.x2() + "," + 
                (int) line.y2();
                writer.append(writeString);
                writer.newLine();
            }

            writer.close();
        }
    }
}
