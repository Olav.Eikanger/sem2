package no.uib.inf101.sem2.model.physics;

public interface PhysicsObject {
    public Vector getVelocity();

    public void setVelocity(Vector velocity);

    public Vector getPosition();

    public void setPosition(Vector position);
}
