package no.uib.inf101.sem2.model;

import java.util.List;
import java.util.Map;

import no.uib.inf101.sem2.model.physics.Line;
import no.uib.inf101.sem2.view.ViewableLevel;

public class LevelModel implements ViewableLevel {
    protected final int WIDTH;
    protected final int HEIGHT;
    private int currentLevel;
    private Map<String, List<Line>> levelCollidables;

    public LevelModel(int currentLevel, Map<String, List<Line>> levelCollidables, int width, int height) {
        this.currentLevel = currentLevel;
        this.levelCollidables = levelCollidables;
        this.WIDTH = width;
        this.HEIGHT = height;
    }

    protected void nextLevel() {
        currentLevel++;
    }

    protected void previousLevel() {
        currentLevel--;
    }

    @Override
    public List<Line> getLevelCollidables() {
        return levelCollidables.get("level" + currentLevel);
    }

    @Override
    public int getCurrentLevel() {
        return currentLevel;
    }

}
