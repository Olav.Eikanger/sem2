package no.uib.inf101.sem2.model.physics;

public record Point(double x, double y) implements Shape {
    @Override
    public Point[] getPoints() {
        return new Point[]{
            new Point(x, y)
        };
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getMaxX() {
        return x;
    }

    @Override
    public double getMaxY() {
        return y;
    }
}
