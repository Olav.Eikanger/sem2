package no.uib.inf101.sem2.model.physics;

public record DistanceToLine(Line line, Vector distance) {}
