package no.uib.inf101.sem2.model;

import java.awt.geom.Rectangle2D;
import java.util.List;

import no.uib.inf101.sem2.controller.ControllableModel;
import no.uib.inf101.sem2.model.physics.CollisionDetection;
import no.uib.inf101.sem2.model.physics.Square;
import no.uib.inf101.sem2.model.physics.Vector;
import no.uib.inf101.sem2.view.ViewableModel;

public class Model implements ViewableModel, ControllableModel {
    private PlayerModel player;
    private LevelModel level;
    private ControllerInputs inputs;
    private CollisionDetectionMethod collisionDetectionMethod;

    public static enum CollisionDetectionMethod {
        DISCRETE, CONTINOUS;
    }

    public Model(
        PlayerModel player,
        LevelModel level,
        ControllerInputs inputs,
        CollisionDetectionMethod collisionDetectionMethod
    ) {
        this.player = player;
        this.level = level;
        this.inputs = inputs;
        this.collisionDetectionMethod = collisionDetectionMethod;
    }

    @Override
    public void doGameTick() {
        PlayerState state = player.getPlayerState();
        state.isOnGround(CollisionDetection.isStandingOnGround(player.getShape(), level.getLevelCollidables()));
        if (!state.isOnGround()) {
            applyGravity();
        } else if (state.isChargingJump()) {
            player.incrementJumpStrength();
            player.setVelocity(Vector.ZERO);
        } else if (!state.isChargingJump() && player.getJumpStrength() != 0) {
            player.performJump(getDirection());
        } else {
            player.walk(getDirection());
        }
        
        switch (collisionDetectionMethod) {
            case CONTINOUS -> checkFutureCollisions();
            case DISCRETE -> {
                player.updatePosition();
                checkCurrentCollisions();
            }
        }
            

        Vector playerPosition = player.getPosition();
        if (playerPosition.y() < 0) {
            level.nextLevel();
            player.setPosition(
                new Vector(
                    playerPosition.x(),
                    level.HEIGHT + (playerPosition.y() % level.HEIGHT)
                )
            );
        } else if (playerPosition.y() > level.HEIGHT) {
            level.previousLevel();
            player.setPosition(
                new Vector(
                    playerPosition.x(),
                    playerPosition.y() % level.HEIGHT
                )
            );
        }
    }

    private void applyGravity() {
        CollisionDetection.applyForce(CollisionDetection.GRAVITY, player);
    }

    private Direction getDirection() {
        if (!(inputs.moveLeft ^ inputs.moveRight)) {
            return Direction.NONE;
        }

        Direction facingDirection = inputs.moveLeft ? Direction.LEFT : Direction.RIGHT;
        return facingDirection;
    }

    private void checkFutureCollisions() {
        int MAX_ITERATIONS = 10;
        Vector remainingVelocity = player.getVelocity();
        Vector[] vectors;
        do {
            vectors = CollisionDetection.checkFutureCollisions(player.getShape(), remainingVelocity, level.getLevelCollidables());
            player.setPosition(player.getPosition().add(vectors[0]));
            remainingVelocity = vectors[1];
            if (!vectors[0].equals(Vector.ZERO)) {
                player.setVelocity(vectors[0].normalized().scaledBy(player.getVelocity().length()));
            }
            MAX_ITERATIONS--;
        } while (!remainingVelocity.equals(Vector.ZERO) && MAX_ITERATIONS > 0);
    }

    private void checkCurrentCollisions() {
        Square playerBounds = player.getShape();
        List<Vector> clipOffsets = CollisionDetection.findAllCollisionOffsets(playerBounds, level.getLevelCollidables());
        clipOffsets.sort((Vector offset1, Vector offset2) -> {
            Vector playerVelocity = player.getVelocity();
            return (int) Math.round(playerVelocity.dot(offset1) - playerVelocity.dot(offset2));
        });


        Vector normal = Vector.ZERO;
        while (
            clipOffsets.size() > 0
            && CollisionDetection.isCollidingWith(player.getShape(), level.getLevelCollidables())
        ) {
            Vector smallestClipOffset = clipOffsets.remove(0);
            player.setPosition(player.getPosition().add(smallestClipOffset));
            if (smallestClipOffset.y() < 0 && smallestClipOffset.x() == 0) {
                continue;
            }

            normal = normal.add(smallestClipOffset.normalized()).normalized();
        }
        
        Vector normalForce = normal.scaledBy(-2 * player.getVelocity().dot(normal));
        CollisionDetection.applyForce(normalForce, player);
    }

    @Override
    public Rectangle2D getPlayerBounds() {
        return player.getBounds();
    }

    @Override
    public int getLevelNumber() {
        return level.getCurrentLevel();
    }

    @Override
    public void setMoveLeft(boolean value) {
        inputs.moveLeft = value;
    }

    @Override
    public void setMoveRight(boolean value) {
        inputs.moveRight = value;
    }

    @Override
    public void chargeJump() {
        inputs.chargeJump = true;
        player.getPlayerState().isChargingJump(true);
    }

    @Override
    public void releaseJump() {
        inputs.chargeJump = false;
        player.getPlayerState().isChargingJump(false);
    }

    @Override
    public int getViewDirection() {
        return getDirection().coefficent;
    }

    @Override
    public boolean playerIsOnGround() {
        return player.getPlayerState().isOnGround();
    }
}
