package no.uib.inf101.sem2.model.physics;

public class Square implements Shape {
    private Point topLeft;
    private Point topRight;
    private Point bottomLeft;
    private Point bottomRight;

    protected enum Side {
        TOP, BOTTOM, LEFT, RIGHT;
    }

    protected Square(Point point1, Point point2, Point point3, Point point4) {
        this.topLeft = point1;
        this.topRight = point1;
        this.bottomLeft = point1;
        this.bottomRight = point1;
        for (Point point : new Point[]{point2, point3, point4}) {
            if (point.x() <= topLeft.x() && point.y() <= topLeft.y()) {
                this.topLeft = point;
            }

            if (point.x() >= topRight.x() && point.y() <= topRight.y()) {
                this.topRight = point;
            }

            if (point.x() <= bottomLeft.x() && point.y() >= bottomLeft.y()) {
                this.bottomLeft = point;
            }

            if (point.x() >= bottomRight.x() && point.y() >= bottomRight.y()) {
                this.bottomRight = point;
            }
        }
    }

    public static Square createRectangle(double x, double y, double width, double height) {
        return new Square(
            new Point(x, y),
            new Point(x + width, y),
            new Point(x + width, y + height),
            new Point(x, y + height)
        );
    }

    @Override
    public Point[] getPoints() {
        return new Point[]{
            topLeft,
            topRight,
            bottomRight,
            bottomLeft
        };
    }

    protected Line getBottomLine() {
        return getSide(Side.BOTTOM);
    }

    protected Line getSide(Side side) {
        return switch (side) {
            case BOTTOM -> new Line(bottomLeft, bottomRight);
            case LEFT -> new Line(topLeft, bottomLeft);
            case RIGHT -> new Line(topRight, bottomRight);
            case TOP -> new Line(topLeft, topRight);
        };
    }

    @Override
    public double getX() {
        return Math.min(topLeft.x(), bottomLeft.x());
    }

    @Override
    public double getY() {
        return Math.min(topLeft.y(), topRight.y());
    }

    @Override
    public double getMaxX() {
        return Math.max(topRight.x(), bottomRight.x());
    }

    @Override
    public double getMaxY() {
        return Math.max(bottomLeft.y(), bottomRight.y());
    }

    /**
     * Gets the width of the square
     * @return The width of the square
     */
    public double getWidth() {
        return getMaxX() - getX();
    }

    /**
     * Gets the height of the square
     * @return The height of the square
     */
    public double getHeight() {
        return getMaxY() - getY();
    }

    public Point getCenter() {
        return new Point(getX() + (getMaxX() - getX()) / 2, getY() + (getMaxY() - getY()) / 2);
    }
}
