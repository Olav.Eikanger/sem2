package no.uib.inf101.sem2.model.physics;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CollisionDetection {
    public static final Vector GRAVITY = new Vector(0, .7);

    /**
     * Gets the minimum collision offset for each line and the rectangle.
     * If a line does not collide with the rectangle, it will not be included
     * in the returned list.
     * @param shape Movable rectangle
     * @param lines Stationary lines
     * @return An unsorted list of collision offsets
     */
    public static List<Vector> findAllCollisionOffsets(Shape shape, List<Line> lines) {
        List<Vector> collisionOffsets = new ArrayList<>();
        for (Line line : lines) {
            Optional<Vector> clipOffset = getMinimumClipOffset(shape, line);
            if (clipOffset.isEmpty()) {
                continue;
            }

            collisionOffsets.add(clipOffset.get());
        }

        return collisionOffsets;
    }

    /**
     * Checks if the bottom line of the rectangle is intercecting with a line with
     * a constant y-value.
     * @param rectangle The rectangle that could be standing on the lines
     * @param lines The lines that could be ground
     * @return true if the rectangle is colliding with a flat line,
     * false otherwise
     */
    public static boolean isStandingOnGround(Square square, List<Line> lines) {
        Line bottomLine = square.getBottomLine();
        for (Line line : lines) {
            if (line.y1() != line.y2()) {
                continue;
            }

            // Fixes a bug where you could sometimes stand on
            // the bottom corner of a block
            if (
                line.x1() <= bottomLine.x1() && line.x2() <= bottomLine.x1()
                || line.x1() >= bottomLine.x2() && line.x2() >= bottomLine.x2()    
            ) {
                continue;
            }

            if (line.y1() == bottomLine.y1() && line.y2() == bottomLine.y2()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the rectangle is colliding with any of the lines
     * @param rectangle
     * @param lines
     * @return true if they are colliding, false otherwise
     */
    public static boolean isCollidingWith(Shape shape, List<Line> lines) {
        for (Line line : lines) {
            if (isCollidingWith(shape, line)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the shape is colliding with the line.
     * @param shape The shape that could collide
     * @param line The collidable line
     * @return True if they are colliding, false otherwise
     */
    public static boolean isCollidingWith(Shape shape, Line line) {
        Line[] axes = new Line[]{
            line,
            getOrthogonol(line)
        };

        for (Line axis : axes) {
            Projection shapeProjection = project(shape, axis);
            Projection lineProjection = project(line, axis);
            if (getMinimumOffsetMagnitude(shapeProjection, lineProjection) == 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets the minimum vector needed to seperate the rectangle from the line, if they are intercecting
     * @param rectangle Movable rectangle
     * @param line2 Stationary line
     * @return An optional containing either the smallest offset-vector or empty if
     * they are not intercecting
     */
    public static Optional<Vector> getMinimumClipOffset(Shape shape, Line line) {
        if (!isCollidingWith(shape, line)) {
            return Optional.empty();
        }

        Line axis = getOrthogonol(line);
        Projection shapeProjection = project(shape, axis);
        Projection lineProjection = project(line, axis);
        double magnitude = getMinimumOffsetMagnitude(shapeProjection, lineProjection);
    
        

        return Optional.of(
            new Vector(
                axis.x2() - axis.x1(),
                axis.y2() - axis.y1()
            )
            .normalized()
            .scaledBy(magnitude)
        );
    }

    private static Projection project(Shape shape, Line axisLine) {
        double minOffset = Double.MAX_VALUE;
        double maxOffset = -Double.MAX_VALUE;
        Vector normalizedAxis =
        new Vector(axisLine.x2() - axisLine.x1(), axisLine.y2() - axisLine.y1()).normalized();
        for (Point point : shape.getPoints()) {
            Vector pointVector = new Vector(point.x(), point.y());
            double offset = normalizedAxis.dot(pointVector);
            minOffset = Math.min(minOffset, offset);
            maxOffset = Math.max(maxOffset, offset);
        }
        
        return new Projection(minOffset, maxOffset);
    }

    private static Line getOrthogonol(Line line) {
        return new Line(
            new Point(-line.y1(), line.x1()),
            new Point(-line.y2(), line.x2())
        );
    }

    private static double getMinimumOffsetMagnitude(Projection projection1, Projection projection2) {
        double diffrence1 = projection2.min() - projection1.max();
        double diffrence2 = projection2.max() - projection1.min();
        double offset = 0;
        if (Math.signum(diffrence1) != Math.signum(diffrence2)) {
            offset = (Math.abs(diffrence1) < Math.abs(diffrence2)) ? diffrence1 : diffrence2;
        }

        return offset;
    }

    private static Vector getMinimumDistance(Shape shape, Line line) {
        Line[] axes = new Line[]{
            line,
            getOrthogonol(line)
        };

        Vector distanceToLine = Vector.ZERO;
        for (Line axis : axes) {
            Projection shapeProjection = project(shape, axis);
            Projection lineProjection = project(line, axis);
            double diffrence1 = lineProjection.min() - shapeProjection.max();
            double diffrence2 = lineProjection.max() - shapeProjection.min();
            if (Math.signum(diffrence1) != Math.signum(diffrence2)) {
                continue;
            }
            
            double distance = (Math.abs(diffrence1) < Math.abs(diffrence2)) ? diffrence1 : diffrence2;
            distanceToLine = distanceToLine.add(
                new Vector(
                    axis.x2() - axis.x1(),
                    axis.y2() - axis.y1()
                )
                .normalized()
                .scaledBy(distance)
            );
        }

        return distanceToLine;
    }

    private static Vector calulateNormal(Point point, Line line) {
        Vector pointVector = new Vector(line.x1() - point.x(), line.y1() - point.y());
        Vector lineVector = new Vector(line.x2() - line.x1(), line.y2() - line.y1()).normalized();
        return pointVector.add(lineVector.scaledBy(-lineVector.dot(pointVector) / lineVector.dot(lineVector)));
    }

    private static double getDistanceBetween(Projection projection1, Projection projection2) {
        double diffrence1 = projection2.min() - projection1.max();
        double diffrence2 = projection2.max() - projection1.min();
        if (Math.signum(diffrence1) != Math.signum(diffrence2)) {
            return 0;
        }
        
        return (Math.abs(diffrence1) < Math.abs(diffrence2)) ? diffrence1 : diffrence2;
    }

    /**
     * Checks if the rectangle given the velocity will collide with the given lines.
     * If so, it calculates a vector to bring the rectangle and line into contact, and a remaining velocity vector
     * that when applied to the rectangle bounces it away properly
     * @param rectangle The rectangle that could collide
     * @param velocity The velocity of the rectangle
     * @param lines A list of collidable lines
     * @return An array of vectors where index 0 contains the vector to that moves the rectangle to the line
     * and index 1 is the remaining velocity post "collision" 
     */
    public static Vector[] checkFutureCollisions(Square rectangle, Vector velocity, List<Line> lines) {
        List<DistanceToLine> distanceToLines = findDistanceToLines(rectangle, lines);
        distanceToLines.sort((DistanceToLine distance1, DistanceToLine distance2) -> {
            Vector directionToLine1 = getMinimumDistance(rectangle.getCenter(), distance1.line());
            Vector directionToLine2 = getMinimumDistance(rectangle.getCenter(), distance2.line());

            if (velocity.dot(directionToLine1) < 0 || velocity.dot(directionToLine2) < 0) {
                return (int) Math.round(velocity.dot(directionToLine2) - velocity.dot(directionToLine1)); 
            }

            return (int) Math.round(velocity.dot(directionToLine1) - velocity.dot(directionToLine2));
        });

        for (DistanceToLine distanceToLine : distanceToLines) {
            // Find the normal vector from the center of the rectangle to the line
            Vector collisionNormal = calulateNormal(rectangle.getCenter(), distanceToLine.line());
            Vector directionToLine = getMinimumDistance(rectangle.getCenter(), distanceToLine.line());
            directionToLine = new Vector(
                (Math.ceil(Math.abs(directionToLine.x())) >= rectangle.getWidth() / 2) ? directionToLine.x() : 0,
                (Math.ceil(Math.abs(directionToLine.y())) >= rectangle.getHeight() / 2) ? directionToLine.y() : 0
            );
            
            if (
                (velocity.dot(new Vector(directionToLine.x(), 0)) <= 0 && directionToLine.x() != 0)
                || (velocity.dot(new Vector(0, directionToLine.y())) <= 0 && directionToLine.y() != 0)
            ) {
                continue;
            }

            // Ignore the cases where the player is impaled by a line.
            // This used to cause a NaN error...
            if (collisionNormal.dot(directionToLine) == 0) {
                continue;
            }

            // Determine the scalar to make the velocity vector reach the line
            double scaleToLine = (distanceToLine.distance().length() != 0)
            ? Math.pow(distanceToLine.distance().length(), 2) / velocity.dot(distanceToLine.distance())
            : 0;

            // If k is greater than 1, then the velocity is not large enough to reach the line
            // If k is smaller than zero, then 
            if (scaleToLine > 1 || scaleToLine < 0) {
                continue;
            }

            // Calculate the speed after the collision
            Vector remainingVelocity = velocity.add(velocity.scaledBy(-scaleToLine));
            collisionNormal = collisionNormal.normalized();
            Vector normalForce = collisionNormal.scaledBy(-remainingVelocity.dot(collisionNormal));
            Vector newVelocity = remainingVelocity.add(normalForce.scaledBy(1.5));

            // If the line is horizontal, i.e. ground, don't bounce
            if (normalForce.y() < 0 && normalForce.x() == 0) {
                return new Vector[]{velocity.scaledBy(scaleToLine), Vector.ZERO};
            }

            return new Vector[]{velocity.scaledBy(scaleToLine), newVelocity};
        }

        return new Vector[]{velocity, Vector.ZERO};
    }

    private static List<DistanceToLine> findDistanceToLines(Square rectangle, List<Line> lines) {
        List<DistanceToLine> distanceToLines = new ArrayList<>();
        for (Line line : lines) {
            Line[] axes = {
                line,
                getOrthogonol(line)
            };

            Vector minDistance = Vector.ZERO;
            for (Line axis : axes) {
                Projection rectangleProjecton = project(rectangle, axis);
                Projection lineProjecton = project(line, axis);
                double distance = getDistanceBetween(rectangleProjecton, lineProjecton);
                if (distance != 0) {
                    minDistance = minDistance.add(new Vector(
                            axis.x2() - axis.x1(),
                            axis.y2() - axis.y1()
                        )
                        .normalized()
                        .scaledBy(distance)
                    );
                }
            }

            distanceToLines.add(new DistanceToLine(line, minDistance));
        }

        return distanceToLines;
    }

    /**
     * Applies a force to the object
     * @param force The force vector
     * @param object The object that's being accelerated
     */
    public static void applyForce(Vector force, PhysicsObject object) {
        object.setVelocity(object.getVelocity().add(force));
    }
}
