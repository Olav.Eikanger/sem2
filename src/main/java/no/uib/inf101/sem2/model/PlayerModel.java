package no.uib.inf101.sem2.model;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.sem2.model.physics.PhysicsObject;
import no.uib.inf101.sem2.model.physics.Square;
import no.uib.inf101.sem2.model.physics.Vector;
import no.uib.inf101.sem2.view.ViewablePlayer;

public class PlayerModel implements PhysicsObject, ViewablePlayer {
    private static double WALK_SPEED = 3;
    private static double JUMP_STRENGTH_INCREMENT = 1;
    private static double MAX_JUMP_STRENGTH = 16;
    private static double HORIZONTAL_JUMP_SPEED = 5.9;
    private static double MAX_FALL_SPEED = 12;

    private Vector position;
    private Vector velocity;
    private Vector dimension;
    private PlayerState state;
    private double jumpStrength;

    public PlayerModel(Vector position, Vector velocity, Vector dimension, PlayerState state) {
        this.position = position;
        this.velocity = velocity;
        this.dimension = dimension;
        this.state = state;
        this.jumpStrength = 0;
    }

    protected void updatePosition() {
        if (velocity.y() > MAX_FALL_SPEED) {
            velocity = new Vector(velocity.x(), MAX_FALL_SPEED);
        }

        position = position.add(velocity);
    }

    @Override
    public Vector getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector position) {
        this.position = position;
    }

    @Override
    public Vector getVelocity() {
        return velocity;
    }

    @Override
    public void setVelocity(Vector velocity) {
        if (velocity.y() > MAX_FALL_SPEED) {
            this.velocity = new Vector(velocity.x(), MAX_FALL_SPEED);
            return;
        }

        this.velocity = velocity;
    }

    @Override
    public Rectangle2D getBounds() {
        return new Rectangle2D.Double(position.x(), position.y(), dimension.x(), dimension.y());
    }

    public Square getShape() {
        return Square.createRectangle(position.x(), position.y(), dimension.x(), dimension.y());
    }

    protected PlayerState getPlayerState() {
        return state;
    }

    protected double getJumpStrength() {
        return jumpStrength;
    }

    protected void setJumpStrength(double value) {
        jumpStrength = value;
    }

    protected void incrementJumpStrength() {
        jumpStrength += JUMP_STRENGTH_INCREMENT;
        if (jumpStrength > MAX_JUMP_STRENGTH) {
            jumpStrength = MAX_JUMP_STRENGTH;
        }
    }

    protected void performJump(Direction direction) {
        velocity = new Vector(direction.coefficent * HORIZONTAL_JUMP_SPEED, -jumpStrength);
        jumpStrength = 0;
        state.isOnGround(false);
    }

    protected void walk(Direction direction) {
        velocity = new Vector(WALK_SPEED * direction.coefficent, 0);
    }
}