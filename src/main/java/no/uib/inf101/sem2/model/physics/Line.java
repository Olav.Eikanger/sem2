package no.uib.inf101.sem2.model.physics;

public record Line(Point point1, Point point2) implements Shape {
    public double x1() {
        return point1.x();
    }

    public double y1() {
        return point1.y();
    }

    public double x2() {
        return point2.x();
    }

    public double y2() {
        return point2.y();
    }
    
    @Override
    public Point[] getPoints() {
        return new Point[]{
            point1,
            point2
        };
    }

    @Override
    public double getX() {
        return Math.min(x1(), x2());
    }

    @Override
    public double getY() {
        return Math.min(y1(), y2());
    }

    @Override
    public double getMaxX() {
        return Math.max(x1(), x2());
    }

    @Override
    public double getMaxY() {
        return Math.max(y1(), y2());
    }

    
}
