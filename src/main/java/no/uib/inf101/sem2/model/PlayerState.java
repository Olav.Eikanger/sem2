package no.uib.inf101.sem2.model;

public class PlayerState {
    private boolean isOnGround;
    private boolean isChargingJump;

    protected boolean isOnGround() {
        return isOnGround;
    }

    protected void isOnGround(boolean value) {
        isOnGround = value;
    }

    protected boolean isChargingJump() {
        return isChargingJump;
    }

    protected void isChargingJump(boolean value) {
        isChargingJump = value;
    }
}
