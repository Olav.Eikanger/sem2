package no.uib.inf101.sem2.model.physics;

public class Projection {
    private final double min;
    private final double max;

    public Projection(double value1, double value2) {
        this.min = Math.min(value1, value2);
        this.max = Math.max(value1, value2);
    }

    public double min() {
        return min;
    }

    public double max() {
        return max;
    }
}
