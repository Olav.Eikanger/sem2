package no.uib.inf101.sem2.model.physics;

public record Vector(double x, double y) implements Comparable<Vector> {
    public static final Vector ZERO = new Vector(0, 0);

    public Vector add(Vector vector) {
        return new Vector(x + vector.x, y + vector.y);
    }

    public Vector add(double x, double y) {
        return new Vector(this.x + x, this.y + y);
    }

    public double length() {
        return Math.hypot(x, y);
    }

    public Vector normalized() {
        double length = length();
        return new Vector(x / length, y / length);
    }

    public Vector scaledBy(double scalar) {
        return new Vector(x * scalar, y * scalar);
    }

    public double dot(Vector vector) {
        return x * vector.x() + y * vector.y();
    }

    @Override
    public int compareTo(Vector other) {
        return (int) (this.length() - other.length());
    }
}
