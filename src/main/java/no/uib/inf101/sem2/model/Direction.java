package no.uib.inf101.sem2.model;

public enum Direction {
    LEFT(-1), RIGHT(1), NONE(0);
    public final int coefficent;

    Direction(int coefficent) {
        this.coefficent = coefficent;
    }
}
