package no.uib.inf101.sem2.model.physics;

public interface Shape {
    /**
     * Gets the points defining the shape
     * @return An array of all the points defining the shape
     */
    public Point[] getPoints();

    /**
     * Gets the minimum x-value of the shape
     * @return A double representing the minimum x-coordinate of the shape
     */
    public double getX();
    
    /**
     * Gets the minimum y-value of the shape
     * @return A double representing the minimum y-coordinate of the shape
     */
    public double getY();
    
    /**
     * Gets the maximum x-value of the shape
     * @return A double representing the maximum x-coordinate of the shape
     */
    public double getMaxX();
    
    
    /**
     * Gets the maximum y-value of the shape
     * @return A double representing the maximum y-coordinate of the shape
     */
    public double getMaxY();
}
